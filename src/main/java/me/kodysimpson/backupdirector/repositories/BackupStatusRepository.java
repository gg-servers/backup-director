package me.kodysimpson.backupdirector.repositories;

import me.kodysimpson.backupdirector.models.BackupStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BackupStatusRepository extends MongoRepository<BackupStatus, String> {

    boolean existsBackupStatusByNodeidEquals(String nodeid);

    BackupStatus findBackupStatusByNodeidEquals(String nodeid);

}
