package me.kodysimpson.backupdirector.repositories;

import me.kodysimpson.backupdirector.models.Backup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BackupRepository extends MongoRepository<Backup, String> {

    Page<Backup> findAllByOrderByDateDesc(Pageable pageable);

}
