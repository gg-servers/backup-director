package me.kodysimpson.backupdirector.controllers;

import me.kodysimpson.backupdirector.models.BackupStatus;
import me.kodysimpson.backupdirector.models.Status;
import me.kodysimpson.backupdirector.repositories.BackupStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/backupstatus")
@CrossOrigin
public class BackupStatusController {

    private final BackupStatusRepository backupStatusRepository;

    @Autowired
    public BackupStatusController(BackupStatusRepository backupStatusRepository) {
        this.backupStatusRepository = backupStatusRepository;
    }

    @GetMapping
    public ResponseEntity<List<BackupStatus>> getBackupStatuss(@RequestParam(name = "location", defaultValue = "all", required = false) String location) {

        if (location.equalsIgnoreCase("all")) {
            return ResponseEntity.ok(backupStatusRepository.findAll());
        } else {
            List<BackupStatus> backupStatuses = backupStatusRepository.findAll();

            return ResponseEntity.ok(backupStatuses.stream()
                    .filter(backupStatus -> backupStatus.getLocation().equalsIgnoreCase(location)).collect(Collectors.toList()));
        }
    }

    @GetMapping("/stats")
    public ResponseEntity<Map<String, Object>> getStats() {

        Map<String, Object> data = new HashMap<>();

        //calculate total backups made
        List<BackupStatus> backups = backupStatusRepository.findAll();
        long totalDone = backups.stream().mapToInt(backupStatus -> backupStatus.getHistory().size()).sum();
        long totalInProgress = backups.stream().filter(backupStatus -> backupStatus.getStatus() == Status.STARTED).count();

        data.put("totalDone", totalDone);
        data.put("totalInProgress", totalInProgress);

        return ResponseEntity.ok(data);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Integer> getBackupStatus(@PathVariable(name = "id") String nodeid) {

        if (backupStatusRepository.existsBackupStatusByNodeidEquals(nodeid)) {
            BackupStatus backupStatus = backupStatusRepository.findBackupStatusByNodeidEquals(nodeid);

            if (backupStatus.getStatus() == Status.DONE) {
                return ResponseEntity.ok(1);
            } else {
                return ResponseEntity.ok(0);
            }
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping("/start")
    public void startBackup(@RequestParam(name = "nodeid") String nodeid, @RequestParam(name = "location") String location) throws Exception {

        //String[] regions = {"na", "eu", "montrealprem", "roubaix", "germany", "london", "finland", "singapore", "australia", "oregon", "virginia"};

        if (!backupStatusRepository.existsBackupStatusByNodeidEquals(nodeid)) {
            BackupStatus backupStatus = new BackupStatus();
            backupStatus.setStatus(Status.STARTED);
            backupStatus.setLocation(location);
            backupStatus.setStartDate(new Date());
            backupStatus.setNodeid(nodeid);

            backupStatusRepository.insert(backupStatus);
        } else {

            BackupStatus backupStatus = backupStatusRepository.findBackupStatusByNodeidEquals(nodeid);

            //see if its finished cuz u cant start something already started boi
            if (backupStatus.getStatus() == Status.DONE) {
                backupStatus.setStatus(Status.STARTED);
                backupStatus.setLocation(location);
                backupStatus.setStartDate(new Date());
                backupStatusRepository.save(backupStatus);
            } else {
                throw new Exception("A backup was already started on this node, it needs to finish first");
            }

        }

    }

    @PostMapping("/finish")
    public void finishBackup(@RequestParam(name = "nodeid") String nodeid) throws Exception {

        if (backupStatusRepository.existsBackupStatusByNodeidEquals(nodeid)) {
            BackupStatus backupStatus = backupStatusRepository.findBackupStatusByNodeidEquals(nodeid);
            backupStatus.setFinishDate(new Date());
            backupStatus.setStatus(Status.DONE);

            backupStatus.getHistory().push(backupStatus);

            backupStatusRepository.save(backupStatus);
        } else {
            throw new Exception("Backup not started on " + nodeid);
        }

    }

}
