package me.kodysimpson.backupdirector.controllers;

import me.kodysimpson.backupdirector.models.Backup;
import me.kodysimpson.backupdirector.repositories.BackupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/backup")
@CrossOrigin("*")
public class WebController {

    private final BackupRepository backupRepository;

    private List<Backup> inProgress = new ArrayList<>();

    @Autowired
    public WebController(BackupRepository backupRepository) {
        this.backupRepository = backupRepository;
    }

    @GetMapping("/ping")
    public String ping() {
        return "pong";
    }

    @GetMapping("/logs")
    public ResponseEntity<Page<Backup>> getBackupLogs(@RequestParam(name = "page", defaultValue = "0") int page,
                                                      @RequestParam(name = "size", defaultValue = "20") int size) {
        return ResponseEntity.ok(backupRepository.findAllByOrderByDateDesc(PageRequest.of(page, size)));
    }

    @PostMapping
    public ResponseEntity<?> doBackup(@RequestParam(name = "nodeid") String nodeID,
                                      @RequestParam(name = "serverid") String serverID,
                                      @RequestParam(name = "when") String when,
                                      @RequestParam(name = "location") String location,
                                      @RequestParam(name = "user", required = false, defaultValue = "admin") String user) throws IOException, InterruptedException {

        System.out.println("Received backup request");
        System.out.println("Node ID: " + nodeID);
        System.out.println("Server ID: " + serverID);
        System.out.println("When: " + when);
        System.out.println("Location: " + location);
        System.out.println("User: " + user);

        if (!nodeID.isEmpty() && !serverID.isEmpty() && !when.isEmpty() && inProgress.isEmpty()) {

            File file = new File("scripts/restore.sh");

            String[] command = {file.getAbsolutePath(), nodeID, serverID, when};

            ProcessBuilder builder = new ProcessBuilder(command);

            Backup backup = new Backup();
            backup.setWhen(when);
            backup.setDate(System.currentTimeMillis());
            backup.setServerID(serverID);
            backup.setNodeID(nodeID);
            backup.setLocation(location);
            backup.setPerp(user);
            backupRepository.insert(backup);

            Process process = builder.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            int exitCode = process.waitFor();

            System.out.println("Exit code: " + exitCode);

            return ResponseEntity.ok("Backup started.");

        } else if (!inProgress.isEmpty()) {
            String error = "Backup Director in use.";
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        } else {
            String error = "Not all values were provided. Try again.";
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
    }

}
