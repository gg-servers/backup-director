package me.kodysimpson.backupdirector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackupDirectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackupDirectorApplication.class, args);
    }

}
