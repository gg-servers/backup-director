package me.kodysimpson.backupdirector.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Backup {

    @Id
    private String id;

    private String location;
    private String nodeID;
    private String serverID;
    private String when; //when the backup is to be downloaded from
    private long date;
    private String perp;

}
