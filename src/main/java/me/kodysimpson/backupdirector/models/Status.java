package me.kodysimpson.backupdirector.models;

public enum Status {

    STARTED, DONE

}
