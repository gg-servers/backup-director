package me.kodysimpson.backupdirector.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Stack;

@Data
@Document
public class BackupStatus {

    @Id
    private String id;

    private String nodeid;
    private String location;

    private Status status;
    private Date startDate;
    private Date finishDate;

    private Stack<BackupStatus> history = new Stack<>();

}
